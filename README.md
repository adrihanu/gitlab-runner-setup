# gitlab-runner-setup
Configuration for `GitLab Runner`.
Configured `docker-compose.yml` with `GitLab Runner` and `Docker in Docker` services.

Tags: `GitLab Runner`, `Docker`, `Docker Compose`

## Running

#### 1. Set path to your project:

```bash
export APP_DIR="/home/adrian/Pulpit/braincore"
```

#### 2.Run services 

```bash
docker-compse up -d
```

#### 3. Register your runner

```bash
docker-compose exec runner gitlab-runner register --docker-privileged --non-interactive --description "???" --url https://gitlab.??? --registration-token ??? --executor docker --docker-image docker:stable
```

#### 4. Congigure .gitlab-ci.yml

#### 5.Run GitLab Runner

```bash
docker-compose exec runner gitlab-runner exec docker --docker-privileged build;
```

